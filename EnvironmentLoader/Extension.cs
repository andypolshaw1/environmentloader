﻿using Microsoft.Extensions.Configuration;

namespace EnvironmentLoader
{
    /// <summary>
    /// Used to make best use of environment variables in apps by merging in with configuration
    /// </summary>
    public static class Extension
    {
        /// <summary>
        /// The environment file to try to load if a different path hasn't been specified
        /// </summary>
        public const string DefaultEnvFileName = ".env";

        /// <summary>
        /// Adds environment variables to the configuration and also imports variables from a custom path OR .env in the root directory of the repo
        /// </summary>
        /// <param name="instance">Instance of <see cref="IConfigurationBuilder"/></param>
        /// <param name="path">The path, relative to the root directory, of the environment file to parse. It looks for a file called .env in the root directory if null</param>
        /// <param name="callAddEnvironmentVariables">Defaukts to true and calls <see cref="EnvironmentVariablesExtensions.AddEnvironmentVariables(IConfigurationBuilder)"/> if true</param>
        /// <returns>The same object passed into <paramref name="instance"/></returns>
        /// <exception cref="PathTooLongException">he specified path, file name, or both exceed the system-defined maximum length.</exception>
        /// <exception cref="IOException">An I/O error occurred while opening the file</exception>
        /// <exception cref="System.Security.SecurityException">The caller does not have the required permission to open the file specified in <paramref name="filePath"/></exception>
        public static IConfigurationBuilder? SetEnvronmentVariables(this IConfigurationBuilder? instance, string? path = null, bool callAddEnvironmentVariables = false)
        {
            var dotenv = Path.Combine(Directory.GetCurrentDirectory(), path ?? DefaultEnvFileName);
            var variableFile = new FileInfo(dotenv);
            return instance?.SetEnvironmentVariables(variableFile, callAddEnvironmentVariables);
        }

        /// <summary>
        /// Adds environment variables to the configuration and also imports variables from a custom path OR .env in the root directory of the repo
        /// </summary>
        /// <param name="instance">Instance of <see cref="IConfigurationBuilder"/></param>
        /// <param name="variablesFile">Details of the file from which to read the environment variablles</param>
        /// <param name="callAddEnvironmentVariables">Defaukts to true and calls <see cref="EnvironmentVariablesExtensions.AddEnvironmentVariables(IConfigurationBuilder)"/> if true</param>
        /// <returns>The same object passed into <paramref name="instance"/></returns>
        public static IConfigurationBuilder? SetEnvironmentVariables(this IConfigurationBuilder? instance, FileInfo variablesFile, bool callAddEnvironmentVariables = false)
        {
            if(variablesFile.Exists)
                return instance.SetEnvironmentVariables(variablesFile.OpenText(), callAddEnvironmentVariables);

            if(callAddEnvironmentVariables)
                return instance?.AddEnvironmentVariables();

            return instance;
        }

        /// <summary>
        /// Adds environment variables to the configuration and also imports variables from a custom path OR .env in the root directory of the repo
        /// </summary>
        /// <param name="instance">Instance of <see cref="IConfigurationBuilder"/></param>
        /// <param name="variables">A <see cref="TextReader"/>  instance from which to read the environment variables</param>
        /// <param name="callAddEnvironmentVariables">Defaukts to true and calls <see cref="EnvironmentVariablesExtensions.AddEnvironmentVariables(IConfigurationBuilder)"/> if true</param>
        /// <returns>The same object passed into <paramref name="instance"/></returns>
        public static  IConfigurationBuilder? SetEnvironmentVariables(this IConfigurationBuilder? instance, TextReader? variables, bool callAddEnvironmentVariables = false)
        {
            if(variables != null)
                Load(variables);

            if(callAddEnvironmentVariables)
                return instance?.AddEnvironmentVariables();
            return instance;
        }

        /// <summary>
        /// Sets environment variables based on key-value  pairs retrieved from (using equals - one line per variable) from <paramref name="variables"/>
        /// </summary>
        /// <param name="variables">A <see cref="TextReader"/> that is read one line at a time</param>
        /// <exception cref="ArgumentNullException"><paramref name="variables"/> is a null reference</exception>
        /// <remarks>The file should contain zero or more lines that look like environmentVariableName = environmentVariableValue</remarks>
        public static void Load(TextReader? variables)
        {
            if(variables==null)
                throw new ArgumentNullException(nameof(variables));

            var line = variables.ReadLine();
            while(line!=null)
            {
                try
                {
                    var parts = line.Split('=', StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length != 2 || parts[0]?.Trim().Length == 0)
                        continue;

                    Environment.SetEnvironmentVariable(parts[0].Trim(), parts[1].Trim());
                }
                finally
                {
                    line = variables.ReadLine();
                }
            }
        }
    }
}