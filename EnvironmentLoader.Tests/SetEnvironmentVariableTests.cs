﻿using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.EnvironmentVariables;
using Moq;
using NUnit.Framework;

namespace EnvironmentLoader.Tests
{
    [TestFixture]
    public class SetEnvironmentVariableTests
    {
        const string Filename = "TestVariables.txt";

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void TestNullBuilder(bool callAddEnvironmentVariables)
        {
            // Arrange 
            IConfigurationBuilder? builder = null;


            // Act
            var returnValue = builder.SetEnvronmentVariables(Filename, callAddEnvironmentVariables);


            // Assert
            returnValue.Should().BeNull();
            Helper.ValidateEnvironmentAganistTestVariables();
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void TestMockedBuilder(bool callAddEnvironmentVariables)
        {
            // Arramge
            var mock = new Mock<IConfigurationBuilder>(MockBehavior.Strict);
            mock.Setup(obj=>obj.Add(It.IsAny<EnvironmentVariablesConfigurationSource>())).Returns(mock.Object);


            // Act
            var returnValue = mock.Object.SetEnvronmentVariables(Filename, callAddEnvironmentVariables);


            // Assert
            returnValue.Should().Be(mock.Object);
            Helper.ValidateEnvironmentAganistTestVariables();
            mock.Verify(obj=>obj.Add(It.IsAny<EnvironmentVariablesConfigurationSource>()), callAddEnvironmentVariables?Times.Once:Times.Never);
        }

        [Test]
        public void TestConfigReplacement()
        {
            // Arrange And Act
            var config = InitConfiguration();


            // Assert
            config?.GetSection("ConnectionStrings")?["TestString"].Should().Be("ReplacedValue");
            Helper.ValidateEnvironmentAganistTestVariables();
        }


        public static IConfiguration? InitConfiguration()
        {
            var config = new ConfigurationBuilder()
                ?.AddJsonFile("appsettings.Tests.json")
                ?.SetEnvronmentVariables(Filename, true)
                ?.Build();
            return config;
        }
    }
}
