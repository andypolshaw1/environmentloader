using System;
using System.IO;
using FluentAssertions;
using NUnit.Framework;

namespace EnvironmentLoader.Tests
{
    [TestFixture]
    public class LoadTests
    {
        const string Filename = "TestVariables.txt";

        [Test]
        [TestCase(Filename, true)]
        [TestCase("MadeUpName.made.up", false)]
        public void TestSuccess(string fileName, bool setsVariables)
        {
            // Arrange
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileName);
            var variablesFile = new FileInfo(path);
            TextReader?  reader = variablesFile.Exists?variablesFile.OpenText(): null;
            ArgumentNullException? exception = null;


            // Act 
            try
            {
                Extension.Load(reader);
            }
            catch (ArgumentNullException ex)
            {
                exception = ex;
            }


            // Assert
            if(setsVariables)
            {
                Helper.ValidateEnvironmentAganistTestVariables();
            }
            else
            {
                exception.Should().NotBeNull();
                exception?.ParamName.Should().Be("variables");
            }
        }
    }
}