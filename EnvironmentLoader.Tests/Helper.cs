﻿using FluentAssertions;
using System;

namespace EnvironmentLoader.Tests
{
    internal static class Helper
    {
        /// <summary>
        /// Checks that the three environment keys are set to the correct values
        /// </summary>
        public static void ValidateEnvironmentAganistTestVariables() 
        {
            Environment.GetEnvironmentVariable("KeyOne").Should().Be("ValueOne", "KeyOne should be ValueOne");
            Environment.GetEnvironmentVariable("KeyTwo").Should().Be("ValueThree", "KeyTwo should be ValueThree");
            Environment.GetEnvironmentVariable("KeyThree").Should().Be(null, "KeyThree should be null");
        }
    }
}
